""" Script to configure and package the DS Tax taxconnect Magento 2 extension
for a specific customer. """
import datetime
import errno
import sys
import os.path
from subprocess import run, PIPE
import time
import os
import shutil
import zipfile

def os_call(arg_list, cwd=None, stdin=None):
    # print and run a command using subprocess call
    for arg in arg_list:
        print(arg + " ", end="")
    print("\n")
    result = run(arg_list, cwd=cwd, stdin=stdin, stdout=PIPE,
                 stderr=PIPE)
    if result.returncode != 0:
        # print stdout and stderr
        print("ERROR:")
        print(result.stdout.decode("utf-8"))
        print(result.stderr.decode("utf-8"))


if sys.version_info[0] < 3:
    print("This script requires Python 3")
else:
    if len(sys.argv) == 2:
        git_repo = sys.argv[1]
    else:
        print("Usage: " + sys.argv[0] + " <path/to/git/repo/>")
        # get current directory
        git_repo = os.path.dirname(os.path.realpath(__file__))
        # sys.exit()
    # get code using git archive to avoid including unneccessary files
    if os.path.exists("packages"):
        print("packages directory already exists")
    else:
        os_call(["mkdir", "packages"])
    repo_dir = git_repo.replace(".git", "")
    assert os.path.isdir(git_repo), "Git repo not found!"
    tar_file = "TaxConnect.tar"
    # os_call(["git", "archive", "--prefix", "TaxConnect/", "-o", tar_file, "master"], cwd=repo_dir)

    # generate .tgz package
    # os_call(["gzip", tar_file])
    targz_file = tar_file + ".gz"
    date_time_string = "-" + time.strftime("%Y-%m-%d-%H%M%S")
    ver_prefix = time.strftime("%y.%m.")
    # os_call(["mv", targz_file, "packages/TaxConnect" + date_time_string + ".tgz"])
    
    # zip2_file = "DSTaxTaxConnect.zip"
    zip_file_name = "TaxConnect_for_WooCommerce"
    folder_name = "TaxConnect"
    
    
    # Open the file in read mode to read the contents
    with open('thomson-reuters-tax.php', 'r') as file:
        lines = file.readlines()
    # with open('thomson-reuters-tax.php', 'r') as file:
    #     lines = file.readlines()
    #     version = lines[6]
    #     version = version.split(" ")[3]
    #     vernum = version.split(".")[2]
    #     newvernum = int(vernum) + 1
    #     newvernum = "{:03d}".format(newvernum)
    #     lines[6] = f" * Version: {ver_prefix + newvernum}\n"

    # Update the version number on line 7
    version = lines[6]
    version = version.split(" ")[3]
    vernum = version.split(".")[2]
    newvernum = int(vernum) + 1
    newvernum = "{:03d}".format(newvernum)
    lines[6] = f" * Version: {ver_prefix + newvernum}\n"
    print(version)
    print(ver_prefix + newvernum)
    input = input("Save as new version? (y/n) or cancel (c)")
    
    if input.lower() == "y":
        # Open the file in write mode to write the updated contents
        date_string = datetime.datetime.now().strftime("%B %d, %Y")
        with open('thomson-reuters-tax.php', 'w') as file:
            file.writelines(lines)
        # Define the folder name and zip file name
        zip_file_name = f"TaxConnect-{ver_prefix + newvernum}"
        folder_name = zip_file_name
        folder_location = "packages/" + folder_name.strip()

        # Define the files/folders to exclude
        exclude_files = ["DEVELOPER.md", "*.zip", ".DS_Store", ".gitignore",folder_location, f"{folder_location}/README.md", f"{folder_location}/uninstall.php", f"{folder_location}/assets/TaxConnectInstallation_GuideWooCommerce.pdf"]
        exclude_folders = ["packages", ".vscode",".git","TaxConnect",folder_name.strip(), folder_location.strip(), f"{folder_location.strip()}/assets",f"{folder_location.strip()}"]
        print(exclude_files)
        print(exclude_folders)
        include_files = ["thomson-reuters-tax.php","README.md","uninstall.php",]
        include_folders = ["assets"]

        # Create a new folder to copy the files into
        if not os.path.exists(f"{folder_name.strip()}"):
            os.makedirs(f"{folder_name.strip()}")

        # Copy all files/folders to the new folder
        for root, dirs, files in os.walk("."):
            for dir in dirs:
                print("dir: " + dir)
                if dir in include_folders and dir not in exclude_folders:
                    src_dir = os.path.join(root, dir)
                    dst_dir = os.path.join(f"{folder_name.strip()}", dir)
                    print("src_dir: " + src_dir)
                    print("dst_dir: " + dst_dir)
                    if src_dir != dst_dir:
                        try:
                            shutil.copytree(src_dir, dst_dir)
                        except Exception as e:
                            print(f"Error copying directory:{dir} \n")
                            print(e.args)
                    else:
                        print("src_dir == dst_dir")
            for file in files:
                if file in include_files and file not in exclude_files:
                    src_file = os.path.join(root, file)
                    dst_file = os.path.join(f"./{folder_name.strip()}", file)
                    if src_file != dst_file:
                        shutil.copy2(src_file, dst_file)
                        # shutil.copytree(src_dir, dst_dir)
                    else:
                        print("src_file == dst_file")

        # Create a ZipFile object with write mode
        with zipfile.ZipFile(zip_file_name, "w") as zip_file:
            # Iterate over all files/folders in the new folder
            for root, dirs, files in os.walk(f"./{folder_name.strip()}"):
                # Add each file to the zip file
                for file in files:
                    zip_file.write(os.path.join(root, file))

        # Move the zip file to the desired location
        os.rename(zip_file_name, os.path.join("packages", zip_file_name.strip() + ".zip"))
        # os_call("mv " + zip_file_name + ".zip packages/" + folder_name + ".zip")

        # Remove the new folder
        shutil.rmtree(f"packages/{folder_name.strip()}")
    elif input.lower() == "n":
        zip_file_name = f"TaxConnect-{ver_prefix + vernum}"
        folder_name = zip_file_name
        folder_location = "packages/" + folder_name.strip()

        # Define the files/folders to exclude
        exclude_files = ["DEVELOPER.md", "*.zip", ".DS_Store", ".gitignore",folder_location, f"{folder_location}/README.md", f"{folder_location}/uninstall.php", f"{folder_location}/assets/TaxConnectInstallation_GuideWooCommerce.pdf"]
        exclude_folders = ["packages", ".vscode",".git","TaxConnect",folder_name.strip(),f"{folder_name.strip()}/assets", folder_location.strip(), f"{folder_location.strip()}/assets",f"{folder_location.strip()}", f"{folder_name.strip()}"]
        print(exclude_files)
        print(exclude_folders)
        include_files = ["thomson-reuters-tax.php","README.md","uninstall.php",]
        include_folders = ["assets"]

        # Create a new folder to copy the files into
        if not os.path.exists(f"{folder_name.strip()}"):
            os.makedirs(f"{folder_name.strip()}")

        # Copy all files/folders to the new folder
        for root, dirs, files in os.walk("."):
            for dir in dirs:
                print("dir: " + dir)
                if dir in include_folders and dir not in exclude_folders:
                    src_dir = os.path.join(root, dir)
                    dst_dir = os.path.join(f"./{folder_name.strip()}", dir)
                    print("src_dir: " + src_dir)
                    print("dst_dir: " + dst_dir)
                    if src_dir != dst_dir:
                        try:
                            shutil.copytree(src_dir, dst_dir)
                        except Exception as e:
                            print("Error copying directory:\n")
                            print(e.args)
                    else:
                        print("src_dir == dst_dir")
            for file in files:
                if file in include_files and file not in exclude_files:
                    src_file = os.path.join(root, file)
                    dst_file = os.path.join(f"./{folder_name.strip()}", file)
                    if src_file != dst_file:
                        shutil.copy2(src_file, dst_file)
                        # shutil.copytree(src_dir, dst_dir)
                    else:
                        print("src_file == dst_file")

        # Create a ZipFile object with write mode
        with zipfile.ZipFile(zip_file_name, "w") as zip_file:
            # Iterate over all files/folders in the new folder
            for root, dirs, files in os.walk(f"{folder_name.strip()}"):
                # Add each file to the zip file
                for file in files:
                    zip_file.write(os.path.join(root, file))

        # Move the zip file to the desired location
        os.rename(zip_file_name, os.path.join("packages", zip_file_name.strip() + ".zip"))
        # os_call("mv " + zip_file_name + ".zip packages/" + folder_name + ".zip")

        # Remove the new folder
        shutil.rmtree(f"./{folder_name.strip()}")
    elif input.lower() == "c":
        print("Cancelled Successfully")
        exit()
    else:
        print("Invalid input")
        exit()
