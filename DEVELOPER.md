# Developer notes for Woocommerce plugin

## Setup a development server
Edit the ansible playbook gitlab.com/dstax/ansible/woocommerce/development.yml
to update the droplet name. Then run:
```
ansible-playbook development.yml
```
The taxable widget will be in the catalog and can be added to cart, but view
cart will not work yet.

Go through Woocommerce install wizard to get checkout working.

## Using wp-cli 
wp-cli is installed by the ansible playbook and used to download, configure and
install wordpress.  This tool is very useful for activating and deactivating
plugins. The plugins on the system can be seen with this command:
```
wp plugin list
```

## Install plugin for development
```
cd /usr/share/nginx/html/wordpress/wp-content/plugins
git clone git@gitlab.com:dstax/woocommerce.git
wp plugin activate thomson-reuters-tax
```

## Create zip file for admin UI installation
```
git archive --prefix=thomson-reuters-tax/ -o thomson_reuters_tax.zip HEAD
```

## Development notes

### Order Number?
I have not been able to get the order number in the calculate\_tax callback
triggered by woocommerce\_after\_calculate\_totals and am thinking that I may need
to try triggering off of other events or possibly setting up an additional
callback to get the order number.
```
do_action( 'woocommerce_before_' . $this->object_type . '_object_save', $this, $this->data_store );
do_action( 'woocommerce_after_' . $this->object_type . '_object_save', $this, $this->data_store );
do_action( 'woocommerce_payment_token_added_to_order', $this->get_id(), $token->get_id(), $token, $token_ids );
do_action( 'woocommerce_order_before_calculate_taxes', $args, $this );
do_action( 'woocommerce_order_before_calculate_totals', $and_taxes, $this );
do_action( 'woocommerce_order_after_calculate_totals', $and_taxes, $this );
```


### Shopping Cart
The shopping cart code is in includes/class-wc-cart.php.

The hooks in this code are:

```
do_action( 'woocommerce_before_calculate_totals', $this );
do_action( 'woocommerce_after_calculate_totals', $this );

do_action( 'woocommerce_add_to_cart', $cart_item_key, $product_id, $quantity,
           $variation_id, $variation, $cart_item_data );

do_action( 'woocommerce_before_cart_emptied', $clear_persistent_cart );
do_action( 'woocommerce_cart_emptied', $clear_persistent_cart );

do_action( 'woocommerce_remove_cart_item', $cart_item_key, $this );
do_action( 'woocommerce_cart_item_removed', $cart_item_key, $this );

do_action( 'woocommerce_restore_cart_item', $cart_item_key, $this );
do_action( 'woocommerce_cart_item_restored', $cart_item_key, $this );

do_action( 'woocommerce_after_cart_item_quantity_update', $cart_item_key,
           $quantity, $old_quantity, $this );
do_action( 'woocommerce_cart_item_set_quantity', $cart_item_key, $quantity, $this );

do_action( 'woocommerce_applied_coupon', $coupon_code );
do_action( 'woocommerce_removed_coupon', $coupon_code );

do_action( 'woocommerce_cart_calculate_fees', $this );

do_action( 'woocommerce_cart_reset', $this, false );
```

I'm thinking after calculate totals sounds promising.


These are the totals calculated in the cart class:

```
protected $default_totals = array(
    'subtotal'            => 0,
    'subtotal_tax'        => 0,
    'shipping_total'      => 0,
    'shipping_tax'        => 0,
    'shipping_taxes'      => array(),
    'discount_total'      => 0,
    'discount_tax'        => 0,
    'cart_contents_total' => 0,
    'cart_contents_tax'   => 0,
    'cart_contents_taxes' => array(),
    'fee_total'           => 0,
    'fee_tax'             => 0,
    'fee_taxes'           => array(),
    'total'               => 0,
    'total_tax'           => 0,
);
```


I added a couple of callbacks and set break points in my call back functions to
see what the state of things are when they get called. As is usual, trying to
see what's going on using VdebugEval on object properties is problematic,
better to use it on methods.

```
:VdebugEval($cart->get_totals())
```

Had to return $cart at the end of the callback function so that subsequent
callbacks work. 

Set woocommerce\_calc\_taxes to yes.
