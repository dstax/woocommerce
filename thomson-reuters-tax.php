<?php
/*
 * Plugin Name: TaxConnect
 * Description: May 8, 2023 - Calculate tax using DSTax.
 * Author: Vu Hoa, Kyle Anderson, 
 * Author URI: https://dstax.com/
 * Version: 23.09.007


 */

// Exit if not running as part of Wordpress.
if (!defined('ABSPATH')) {
    exit;
}

// Insert row in woocommerce_tax_rates with tax_rate_name='Tax'
// because tax_rate_name appears in the cart.
register_activation_hook(__FILE__, 'activate');

function activate()
{
    global $wpdb;

    $rate = $wpdb->insert(
        $wpdb->prefix . 'woocommerce_tax_rates',
        array(
            'tax_rate_class' => 'Thomson-Reuters',
            'tax_rate_name' => 'Tax',
        )
    );
}

// Delete rows in woocommerce_tax_rates with tax_rate_class='Thomson-Reuters'
register_deactivation_hook(__FILE__, 'deactivate');

function deactivate()
{
    global $wpdb;

    $rate = $wpdb->query(
        "DELETE FROM " . $wpdb->prefix . "woocommerce_tax_rates " .
            "WHERE tax_rate_class='Thomson-Reuters';"
    );
}


if (!function_exists('plugin_log')) {
    function plugin_log($entry, $mode = 'a', $file = 'taxes')
    {
        // Get WordPress uploads directory.
        $upload_dir = wp_upload_dir();
        $upload_dir = $upload_dir['basedir'];
        // If the entry is array, json_encode.
        if (is_array($entry)) {
            $entry = json_encode($entry);
        }
        // Write the log file.
        $file  = $upload_dir . '/' . $file . '.log';
        $file  = fopen($file, $mode);
        $bytes = fwrite($file, current_time('mysql') . "::" . $entry . "\n");
        fclose($file);
        return $bytes;
    }
}


function add_tax_to_order($order, $total_tax)
{
    // // Populate relevant WooCommerce fields with tax
    plugin_log($order->order_type);
    global $wpdb;
    $tax_rate_id = $wpdb->get_var(
        "SELECT tax_rate_id FROM " . $wpdb->prefix . "woocommerce_tax_rates " .
            "WHERE tax_rate_name='Tax';"
    );
    $item = new WC_Order_Item_Tax();
    $item->set_rate($tax_rate_id);
    $item->set_tax_total($total_tax);
    $item->set_shipping_tax_total(0);
    $order->add_item($item);
    $order->update_taxes();

    plugin_log("total_tax: " . $order->get_total_tax());
    plugin_log("total discount: " . $order->get_total_discount());
    plugin_log("total fees: " . $order->get_total_fees());
    plugin_log("total shipping: " . $order->get_total_shipping());
    plugin_log("total tax: " . $order->get_total_tax());
    plugin_log("subtotal: " . $order->get_subtotal());
    plugin_log("total: " . $order->get_total());
    plugin_log("----------");

    $new_order_total = $order->get_total_discount() + $order->get_total_fees() + $order->get_total_shipping() + $order->get_total_tax() + $order->get_subtotal();
    $order->set_total($new_order_total + $total_tax);
    $order->save();

    plugin_log("AFTER UPDATED");
    plugin_log("total_tax: " . $order->get_total_tax());
    plugin_log("total discount: " . $order->get_total_discount());
    plugin_log("total fees: " . $order->get_total_fees());
    plugin_log("total shipping: " . $order->get_total_shipping());
    plugin_log("total tax: " . $order->get_total_tax());
    plugin_log("subtotal: " . $order->get_subtotal());
    plugin_log("total: " . $order->get_total());
    plugin_log("----------");
}

add_action('woocommerce_order_after_calculate_totals', "custom_order_after_calculate_totals", 10, 2);
function custom_order_after_calculate_totals($and_taxes, $order)
{
    plugin_log("woocommerce_order_after_calculate_totals-start");
    if (get_class($order) != 'WC_Order_Refund') {
        $response = subscription_tax_calculation($order);
        if (is_array($response)) {
            $taxResponse = json_decode($response['body']);

            // Populate relevant WooCommerce fields with tax
            $total_tax = $taxResponse->TotalTaxAmount;

            // Populate relevant WooCommerce fields with tax
            add_tax_to_order($order, $total_tax);
        }
    }
    plugin_log("woocommerce_order_after_calculate_totals-ended");
}

add_action('woocommerce_checkout_subscription_created', 'woocommerce_checkout_subscription_created', 1, 3);
add_action('woocommerce_scheduled_subscription_payment', 'subscription_created');
add_action('woocommerce_subscription_renewal_payment_complete', 'woocommerce_subscription_renewal_payment_complete', 1, 2);
add_action('woocommerce_subscription_payment_complete', 'woocommerce_subscription_payment_complete', 1, 2);

function subscription_created($subscription_id)
{
    plugin_log("subscription_created-start");
    $subscription = new WC_Subscription($subscription_id);
    $response = subscription_tax_calculation($subscription);
    if (is_array($response)) {
        $taxResponse = json_decode($response['body']);

        // Populate relevant WooCommerce fields with tax
        $total_tax = $taxResponse->TotalTaxAmount;

        add_tax_to_order($subscription, $total_tax);

        plugin_log("subscription_created-ended");
    }
}

function woocommerce_checkout_subscription_created($subscription, $order, $recurring_cart)
{
    plugin_log("woocommerce_checkout_subscription_created-start");
    // if(get_option('wc_thomson_reuters_itbridge_audit')=='on')
    //     { $audit = true;}
    // else{ $audit= false;}
    $audit = false;
    $response = subscription_tax_calculation($subscription, $audit);
    if (is_array($response)) {
        $taxResponse = json_decode($response['body']);

        // Populate relevant WooCommerce fields with tax
        $total_tax = $taxResponse->TotalTaxAmount;

        # update subscription
        add_tax_to_order($subscription, $total_tax);

        # update order
        add_tax_to_order($order, $total_tax);

        plugin_log("woocommerce_checkout_subscription_created-ended");
    }
}

function woocommerce_subscription_payment_complete($subscription)
{
    plugin_log("woocommerce_subscription_payment_complete-start");

    $order_id = strval($subscription->get_parent_id());
    $suborder_id = strval($subscription->get_id());
    if ($order_id == $suborder_id) {
        if (get_option('wc_thomson_reuters_itbridge_audit') == 'on') {
            $audit = true;
        } else {
            $audit = false;
        }
        $response = subscription_tax_calculation($subscription, $audit);

        if (is_array($response)) {
            $taxResponse = json_decode($response['body']);

            // Populate relevant WooCommerce fields with tax
            $total_tax = $taxResponse->TotalTaxAmount;

            # update subscription
            add_tax_to_order($subscription, $total_tax);

            // # update order
            // add_tax_to_order($order, $total_tax);

            plugin_log("woocommerce_subscription_payment_complete-ended");
        }
    } else {
        plugin_log("woocommerce_subscription_payment_complete-ended");
    }
}


function woocommerce_subscription_renewal_payment_complete($subscription, $order)
{
    plugin_log("woocommerce_subscription_renewal_payment_complete-start");

    if (get_option('wc_thomson_reuters_itbridge_audit') == 'on') {
        $audit = true;
    } else {
        $audit = false;
    }
    $response = subscription_tax_calculation($subscription);
    if (is_array($response)) {
        $taxResponse = json_decode($response['body']);

        // Populate relevant WooCommerce fields with tax
        $total_tax = $taxResponse->TotalTaxAmount;

        # update subscription
        add_tax_to_order($subscription, $total_tax);

        # update order
        add_tax_to_order($order, $total_tax);

        plugin_log("woocommerce_subscription_renewal_payment_complete-ended");
    }
}


function woocommerce_subscription_status_updated($subscription, $new_status, $old_status)
{
    plugin_log("woocommerce_subscription_status_updated-start");

    $response = subscription_tax_calculation($subscription);

    if (is_array($response)) {
        $taxResponse = json_decode($response['body']);

        // Populate relevant WooCommerce fields with tax
        $total_tax = $taxResponse->TotalTaxAmount;

        # update subscription
        $subscription->status_transition = false;
        add_tax_to_order($subscription, $total_tax);
        plugin_log("woocommerce_subscription_status_updated-ended");
    }
}


add_action('woocommerce_subscription_status_updated', 'woocommerce_subscription_status_updated', 10, 3);


function debug_to_console($data)
{
    $output = $data;
    if (is_array($output))
        $output = implode(',', $output);

    echo "<script>console.log('Debug Objects: " . $output . "' );</script>";
}

// Calculate tax quote after cart totals are calculated.
add_action('woocommerce_after_calculate_totals', 'update_cart_tax');

function update_cart_tax($cart)
{
    global $wpdb;
    plugin_log("update_cart_tax");

    $response = tax_calculation_request($cart);
    if (is_array($response)) {
        $taxResponse = json_decode($response['body']);

        // Populate relevant WooCommerce fields with tax
        $total_tax = $taxResponse->TotalTaxAmount;
        $tax_rate_id = $wpdb->get_var(
            "SELECT tax_rate_id FROM " . $wpdb->prefix . "woocommerce_tax_rates " .
                "WHERE tax_rate_name='Tax';"
        );
        $cart->set_cart_contents_taxes(array($tax_rate_id => $total_tax));
        $cart->set_cart_contents_tax($total_tax);
        $cart->set_subtotal_tax($total_tax);
        // $cart->set_shipping_tax( $value );
        // $cart->set_shipping_taxes( $value );
        // $cart->set_discount_tax( $value );
        // $cart->set_fee_tax( $value );
        // $cart->set_fee_taxes( $value );
        $cart->set_total_tax($total_tax);
        $cart->set_total($cart->get_total($context = 'model') + $total_tax);
    }
    return $cart;
}

// Calculate final tax after order is placed.
$priority = 10;
$num_args = 2;
add_action('woocommerce_checkout_update_order_meta', 'tax_call_with_order_id', $priority, $num_args);
add_action('woocommerce_order_refunded', 'refund_tax_call_with_order_id', $priority, $num_args);
function tax_call_with_order_id($order_id, $data)
{
    plugin_log("tax_call_with_order_id");
    $order = wc_get_order($order_id);
    $cart = WC()->cart;
    assert($cart->get_cart_hash() == $order->get_cart_hash());
    $audit = false;
    if (get_option('wc_thomson_reuters_itbridge_audit') == 'on') {
        $audit = true;
    }


    tax_calculation_request($cart, $order_id, $audit);

    return array($order_id, $data);
}

function refund_tax_call_with_order_id($order_id, $data)
{
    plugin_log("refund_tax_call_with_order_id");
    $order = wc_get_order($order_id);
    $cart = WC()->cart;
    assert($cart->get_cart_hash() == $order->get_cart_hash());
    if (get_option('wc_thomson_reuters_itbridge_audit') == 'on') {
        $audit = true;
    } else {
        $audit = false;
    }



    tax_calculation_request($cart, $order_id, $audit, true);

    return array($order_id, $data);
}


//start new subscription tax calc code
function subscription_tax_calculation($subscription, $audit = false)
{
    global $wpdb;

    // Get customer info
    //$customer = $subscription->get_customer();

    // Create an array for billing and shipping addresses
    $addresses = array();

    // Add billing address
    $addresses['BillToAddress'] = array(
        'Country' => $subscription->get_billing_country(),
        'Region' => $subscription->get_billing_state(),
        'City' => $subscription->get_billing_city(),
        'PostalCode' => get_postal_code(
            $subscription->get_billing_country(),
            $subscription->get_billing_postcode(),
        ),
        'GeoCode' => get_geo_code(
            $subscription->get_billing_country(),
            $subscription->get_billing_postcode(),
        ),
    );

    //// Add ship to address
    $addresses['ShipToAddress'] = array(
        'Country' => $subscription->get_shipping_country(),
        'Region' => $subscription->get_shipping_state(),
        'City' => $subscription->get_shipping_city(),
        'PostalCode' => get_postal_code(
            $subscription->get_shipping_country(),
            $subscription->get_shipping_postcode(),
        ),
        'GeoCode' => get_geo_code(
            $subscription->get_shipping_country(),
            $subscription->get_shipping_postcode(),
        ),
    );

    $addresses['ShipToAddress']['PartnerNumber'] = $subscription->get_customer_id();

    // construct an array of items in the cart for tax calculation
    // $subtotal =$subscription->get_total_discount() + $subscription->get_total_fees() + $subscription->get_total_shipping() + $subscription->get_subtotal();

    $total_shipping = (float) $subscription->get_total_shipping();
    $subtotal = (float) $subscription->get_total_discount() + $subscription->get_total_fees() + $subscription->get_subtotal();
    $total_discount = (float) $subscription->get_total_discount();
    $lineItems = [];
    if (get_class($subscription) != 'WC_Order') {
        $items = $subscription->get_items();

        plugin_log('class: ' . get_class($subscription));
        if (count($items) > 0) {
            foreach ($items as $item) {
                plugin_log('item ' . $item);
                // amortize shipping and discounts over all items in the cart
                $price = (float) $item['subtotal'];
                $quantity = (float) $item['quantity'];
                $unit_price = (float) $price / $quantity;
                $discount = (float) $total_discount * ($unit_price / $subtotal);
                $unit_shipping = (float) $total_shipping * ($unit_price / $subtotal);
                $sku = $wpdb->get_var(
                    "SELECT sku FROM " . $wpdb->prefix . "wc_product_meta_lookup " .
                        "WHERE product_id=" . $item['product_id'] . ";"
                );
                array_push($lineItems, [
                    'discount'         => $discount,
                    'product_code'     => $item['id'],
                    'quantity'         => $item['quantity'],
                    'sku'              => $sku,
                    'unit_price'       => $unit_price,
                    'unit_shipping'    => $unit_shipping,
                ]);
            }
        }

        $fees = $subscription->get_fees();
        $total_fees = 0.0;
        if (count($fees) > 0) {
            foreach ($fees as $fee) {
                plugin_log('fee ' . $fee);
                // amortize shipping and discounts over all items in the cart
                $taxable = $fee['tax_status'];
                if ($taxable == 'taxable') {
                    $total_fees += $fee['amount'];
                    array_push($lineItems, [
                        'product_code'     => $fee['id'],
                        'quantity'         => 1,
                        'sku'              => $fee['id'],
                        'unit_price'       => $fee['amount'],
                        'unit_shipping'    => 1,
                    ]);
                }
            }
        }
    } else {
        plugin_log('class: ' . get_class($subscription));
        $items = $subscription->get_cart_contents();

        plugin_log('len items ' . count($items));
        if (count($items) > 0) {
            foreach ($items as $item) {
                plugin_log('item ' . $item);
                // amortize shipping and discounts over all items in the cart
                $unit_price = (float) $item['data']->get_price();
                $discount = (float) $total_discount * ($unit_price / $subtotal);
                $unit_shipping = (float) $total_shipping * ($unit_price / $subtotal);
                $sku = $wpdb->get_var(
                    "SELECT sku FROM " . $wpdb->prefix . "wc_product_meta_lookup " .
                        "WHERE product_id=" . $item['product_id'] . ";"
                );
                array_push($lineItems, [
                    'discount'         => $discount,
                    'product_code'     => $item['data']->get_tax_class(),
                    'quantity'         => $item['quantity'],
                    'sku'              => $sku,
                    'unit_price'       => $unit_price,
                    'unit_shipping'    => $unit_shipping,
                ]);
            }
        }

        $fees = $subscription->get_fees();
        $total_fees = 0.0;
        if (count($fees) > 0) {
            foreach ($fees as $fee) {
                plugin_log('fee ' . $fee);
                // amortize shipping and discounts over all items in the cart
                $taxable = $fee->taxable;
                if ($taxable) {
                    $total_fees += $fee->amount;
                    array_push($lineItems, [
                        'product_code'     => $fee->tax_class,
                        'quantity'         => 1,
                        'sku'              => $fee->id,
                        'unit_price'       => $fee->amount,
                        'unit_shipping'    => 1,
                    ]);
                }
            }
        }
    }

    $order_id = strval($subscription->get_parent_id());
    $suborder_id = strval($subscription->get_id());


    $tax_data = array(
        'addresses' => $addresses,
        // 'customer_vat' => $quote->getCustomerTaxvat(),
        'items' => $lineItems,
        'parent_number' => $order_id,
        'order_number' => $suborder_id,
        'return_logs' => true,
        'total_shipping' => (float) $total_shipping,
        'total_subtotal' => (float) $subtotal,
        'tr_invoice_number_format' => true,
        'post_to_audit' => $audit,
    );

    $user_id = $subscription->get_customer_id();
    $tax_status = get_user_meta($user_id, 'tax-status');
    if ($tax_status) {
        $tax_data['user_attributes'] = ['10' => $tax_status[0]];
    }

    // end of new code
    // Setup HTTP headers
    $api_key = get_option('wc_thomson_reuters_api_key');
    $headers = array(
        'Authorization' => 'Bearer ' . $api_key,
        'Accept' => 'application/json',
        'Content-Type' => 'application/json'
    );
    // Make tax call
    $itbridge_env = get_option('wc_thomson_reuters_itbridge_env');
    if (preg_match("/uat/", $itbridge_env)) {
        $url = "https://uat.itbridge.dstax.com/api/v0/tax";
    } elseif (preg_match("/prodportal/", $itbridge_env)) {
        $url = "https://portal.dstax.com/api/v0/tax/";
    } elseif (preg_match("/portal/", $itbridge_env)) {
        $url = "https://uat.portal.dstax.com/api/v0/tax/";
    } elseif (preg_match("/nexus/", $itbridge_env)) {
        $url = "https://nexus.itbridge.dstax.com/api/v0/tax";
    } elseif (preg_match("/vertex/", $itbridge_env)) {
        $url = "https://nexus.itbridge.dstax.com/api/v1/tax";
    } else {
        $url = "https://itbridge.dstax.com/api/v0/tax";
    }
    $response = wp_safe_remote_post(
        $url,
        array(
            'method'  => 'POST',
            'headers' => $headers,
            'body'    => json_encode($tax_data),
            'timeout' => 70,
        )
    );

    if (!(is_array($response)) && (get_class($response) == 'WP_Error')) {
        // tax call failed try it again with http
        // This is a work around since itbridge.dstax.com currently only
        // supports http. This will not be necessary when it supports https.
        $url = str_replace('https', 'http', $url);
        $response = wp_safe_remote_post(
            $url,
            array(
                'method'  => 'POST',
                'headers' => $headers,
                'body'    => json_encode($tax_data),
                'timeout' => 70,
            )
        );
    }
    plugin_log('body: ' . json_encode($tax_data));
    // Log XML tax request and response, if logging is enabled
    $logging = get_option('wc_thomson_reuters_logging');
    // plugin_log($response);
    // plugin_log($tax_data);
    if ($logging) {
        if (is_array($response)) {
            // $taxes_logger = new WC_Logger();
            // $taxes_logger->log('critical', $response['body']);

            // $taxResponse = json_decode($response['body']);

            // $taxes_logger->log('critical', $taxResponse->xml_request);
            // $taxes_logger->log('critical', $taxResponse->xml_response);
            // $taxes_logger->log('critical', $taxResponse->TotalTaxAmount);
        }
    }
    return $response;
}

//end of new subscription code

function tax_calculation_request($cart, $order_id = 'Quote', $audit = false, $refund = false)
{
    global $wpdb;


    // Get customer info
    $customer = $cart->get_customer();

    // Create an array for billing and shipping addresses
    $addresses = array();

    // Add billing address
    $addresses['BillToAddress'] = array(
        'Country' => $customer->get_billing_country(),
        'Region' => $customer->get_billing_state(),
        'City' => $customer->get_billing_city(),
        'PostalCode' => get_postal_code(
            $customer->get_billing_country(),
            $customer->get_billing_postcode()
        ),
        'GeoCode' => get_geo_code(
            $customer->get_billing_country(),
            $customer->get_billing_postcode()
        ),
    );

    //// Add ship to address
    $addresses['ShipToAddress'] = array(
        'Country' => $customer->get_shipping_country(),
        'Region' => $customer->get_shipping_state(),
        'City' => $customer->get_shipping_city(),
        'PostalCode' => get_postal_code(
            $customer->get_shipping_country(),
            $customer->get_shipping_postcode()
        ),
        'GeoCode' => get_geo_code(
            $customer->get_shipping_country(),
            $customer->get_shipping_postcode()
        ),
    );

    $addresses['ShipToAddress']['PartnerNumber'] = $customer->get_id();
    $addresses['ShipToAddress']['PartnerName'] = $customer->get_first_name()
        . ' ' . $customer->get_last_name();

    // construct an array of items in the cart for tax calculation
    $subtotal = $cart->get_subtotal();
    # if refund make the subtotal negative
    if ($refund) {
        $subtotal = -1.0 * $subtotal;
    }
    $total_shipping = (float) $cart->get_shipping_total();
    $total_discount = (float) $cart->get_discount_total();
    $lineItems = [];
    $subscription_items = [];
    $items = $cart->get_cart_contents();
    plugin_log('items: ' . json_encode($items));
    if (count($items) > 0) {
        foreach ($items as $item) {
            // amortize shipping and discounts over all items in the cart
            $unit_price = (float) $item['data']->get_price();
            $discount = (float) $total_discount * ($unit_price / $subtotal);
            $unit_shipping = (float) $total_shipping * ($unit_price / $subtotal);
            $sku = $wpdb->get_var(
                "SELECT sku FROM " . $wpdb->prefix . "wc_product_meta_lookup " .
                    "WHERE product_id=" . $item['product_id'] . ";"
            );
            array_push($lineItems, [
                'discount'         => $discount,
                'product_code'     => $item['data']->get_tax_class(),
                'quantity'         => $item['quantity'],
                'sku'              => $sku,
                'unit_price'       => $unit_price,
                'unit_shipping'    => $unit_shipping,
            ]);
        }
    }
    $fees = $cart->get_fees();
    plugin_log('fees: ' . json_encode($fees));
    $total_fees = 0.0;
    if (count($fees) > 0) {
        foreach ($fees as $fee) {
            // amortize shipping and discounts over all items in the cart
            plugin_log('fee: ' . json_encode($fee));
            $taxable = $fee->taxable;
            if ($taxable) {
                $total_fees += $fee->amount;
                array_push($lineItems, [
                    'product_code'     => $fee->tax_class,
                    'quantity'         => 1,
                    'sku'              => $fee->id,
                    'unit_price'       => $fee->amount,
                    'unit_shipping'    => 1,

                ]);
            }
        }
    }
    $tax_data = array(
        'addresses' => $addresses,
        // 'customer_vat' => $quote->getCustomerTaxvat(),
        'items' => $lineItems,
        'order_number' => $order_id,
        'return_logs' => true,
        'total_shipping' => $total_shipping,
        'total_subtotal' => $cart->get_subtotal() + $total_fees,
        'tr_invoice_number_format' => true,
        'post_to_audit' => $audit,
    );

    $user_id = $customer->get_id();
    $tax_status = get_user_meta($user_id, 'tax-status');
    if ($tax_status) {
        $tax_data['user_attributes'] = ['10' => $tax_status[0]];
    }

    plugin_log('tax_data: ' . json_encode($tax_data));
    // Setup HTTP headers
    $api_key = get_option('wc_thomson_reuters_api_key');
    $headers = array(
        'Authorization' => 'Bearer ' . $api_key,
        'Accept' => 'application/json',
        'Content-Type' => 'application/json'
    );
    plugin_log('headers: ' . json_encode($headers));
    // Make tax call
    $itbridge_env = get_option('wc_thomson_reuters_itbridge_env');
    if (preg_match("/uat/", $itbridge_env)) {
        $url = "https://uat.itbridge.dstax.com/api/v0/tax";
    } elseif (preg_match("/prodportal/", $itbridge_env)) {
        $url = "https://portal.dstax.com/api/v0/tax/";
    } elseif (preg_match("/portal/", $itbridge_env)) {
        $url = "https://uat.portal.dstax.com/api/v0/tax/";
    } elseif (preg_match("/nexus/", $itbridge_env)) {
        $url = "https://nexus.itbridge.dstax.com/api/v0/tax";
    } elseif (preg_match("/vertex/", $itbridge_env)) {
        $url = "https://nexus.itbridge.dstax.com/api/v1/tax";
    } else {
        $url = "https://itbridge.dstax.com/api/v0/tax";
    }
    $response = wp_safe_remote_post(
        $url,
        array(
            'method'  => 'POST',
            'headers' => $headers,
            'body'    => json_encode($tax_data),
            'timeout' => 70,
        )
    );
    plugin_log('url: ' . $url);
    plugin_log('body: ' . json_encode($tax_data));
    plugin_log('env: ' . $itbridge_env);

    plugin_log('response: ' . json_encode($response['body']));
    if (!(is_array($response)) && (get_class($response) == 'WP_Error')) {
        // tax call failed try it again with http
        // This is a work around since itbridge.dstax.com currently only
        // supports http. This will not be necessary when it supports https.
        $url = str_replace('https', 'http', $url);
        $response = wp_safe_remote_post(
            $url,
            array(
                'method'  => 'POST',
                'headers' => $headers,
                'body'    => json_encode($tax_data),
                'timeout' => 70,
            )
        );
    }

    // Log XML tax request and response, if logging is enabled
    $logging = get_option('wc_thomson_reuters_logging');
    // plugin_log($response);
    // plugin_log($tax_data);
    if ($logging) {
        if (is_array($response)) {
            // $taxes_logger = new WC_Logger();
            // $taxes_logger->log('critical', $response['body']);
            // $taxResponse = json_decode($response['body']);

            // // $core_logger->clear_expired_logs();  // might clear other logs?
            // $taxes_logger->log('critical', $taxResponse->xml_request);
            // $taxes_logger->log('critical', $taxResponse->xml_response);
            // $taxes_logger->log('critical', $taxResponse->TotalTaxAmount);
        }
    }
    return $response;
}

//Tax call for subscription
//
//




// Return first 5 digits of postal code for US addresses
function get_postal_code($country, $postcode)
{
    if ($country == 'US') {
        $postalCode = substr($postcode, 0, 5);
    } else {
        $postalCode = $postcode;
    }
    return $postalCode;
}

// Return last 4 digits of postal code for US addresses
function get_geo_code($country, $postcode)
{
    if ($country == 'US') {
        $geoCode = substr($postcode, -4, 4);
    } else {
        $geoCode = null;
    }
    return $geoCode;
}

// Create admin UI
add_action('admin_menu', 'admin_ui');

function admin_ui()
{
    add_menu_page(
        'Tax',
        'TaxConnect',
        'manage_options',
        'thomson-reuters-tax',
        'display_settings_page'
    );
}

function display_settings_page()
{
    if (!current_user_can('manage_options')) {
        wp_die(__('You do not have sufficient permissions to access this page.'));
    }

    // Save settings on form submission
    $api_key_option = 'wc_thomson_reuters_api_key';
    $logging_option = 'wc_thomson_reuters_logging';
    $itbridge_env_option = 'wc_thomson_reuters_itbridge_env';
    $itbridge_audit_option = 'wc_thomson_reuters_itbridge_audit';
    if (isset($_POST['update_settings']) && $_POST['update_settings'] == 'Y') {
        // Save the posted value in the database
        update_option($api_key_option, $_POST['api_key']);
        update_option($logging_option, $_POST['logging']);
        update_option($itbridge_env_option, $_POST['itbridge_env']);
        update_option($itbridge_audit_option, $_POST['audit']);

        // Put a "settings saved" message on the screen
?>
        <div class="updated">
            <p><strong><?php _e('settings saved.'); ?></strong></p>
        </div>
    <?php
    }

    // Read in option value from database
    $api_key = get_option($api_key_option);
    $logging = get_option($logging_option);
    $itbridge_env = get_option($itbridge_env_option);
    $audit = get_option($itbridge_audit_option);

    ?>
    <form name="tr-tax-settings" method="post" action="">
        <input type="hidden" name="update_settings" value="Y">
        <div class="wrap">
            <!-- <h1>Thomson Reuters Tax Settings</h1> -->
            <h1>TaxConnect Settings</h1>
        </div><!-- /.wrap -->
        <table class="form-table">
            <tbody>
                <tr class="elementor_default_generic_fonts">
                    <th scope="row">API Key</th>
                    <td>
                        <input type="text" name="api_key" value="<?php echo $api_key; ?>" class="regular-text">
                        <p class="description">API key for making calls to Thomson-Reuters tax calculation service.</p>
                    </td>
                </tr>
                <tr class="elementor_default_generic_fonts">
                    <th scope="row">Environment</th>
                    <td>
                        <select name="itbridge_env">
                            <option value="portal" <?php if (preg_match("/portal/", $itbridge_env)) {
                                                        echo 'selected';
                                                    } ?>>
                                User Acceptance Testing - PORTAL
                            </option>
                            <option value="prodportal" <?php if (preg_match("/prodportal/", $itbridge_env)) {
                                                            echo 'selected';
                                                        } ?>>
                                Production - PORTAL
                            </option>
                            <option value="nexus" <?php if (preg_match("/nexus/", $itbridge_env)) {
                                                        echo 'selected';
                                                    } ?>>
                                Nexus Testing
                            </option>
                            <option value="vertex" <?php if (preg_match("/vertex/", $itbridge_env)) {
                                                        echo 'selected';
                                                    } ?>>
                                Nexus Vertex Testing
                            </option>
                            <option value="uat" <?php if (preg_match("/uat/", $itbridge_env)) {
                                                    echo 'selected';
                                                } ?>>
                                User Acceptance Testing - ITBRIDGE
                            </option>
                            <option value="" <?php if (preg_match("/^$/", $itbridge_env)) {
                                                    echo 'selected';
                                                } ?>>
                                Production - ITBRIDGE
                            </option>
                        </select>
                        <p class="description">Testing or production environment.</p>
                    </td>
                </tr>
                <tr class="elementor_default_generic_fonts">
                    <th scope="row">Logging</th>
                    <td>
                        <input type="checkbox" name="logging" <?php if ($logging) {
                                                                    echo 'checked="yes"';
                                                                } ?>>
                        <p class="description">Enable tax call logging.</p>
                    </td>
                </tr>
                <tr class="elementor_default_generic_fonts">
                    <th scope="row">Audit final Order</th>
                    <td>
                        <input type="checkbox" name="audit" <?php if ($audit) {
                                                                echo 'checked="yes"';
                                                            } ?>>
                        <p class="description">Make audit call on Order complete.</p>
                    </td>
                </tr>
            </tbody>
        </table>
        <?php
        submit_button();
        ?>
    </form>
<?php
}
