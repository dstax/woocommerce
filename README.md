# DSTax TaxConnect Plugin for Woocommerce 

## Installation using the administrative user interface (Admin UI)
* Go to your Admin UI, which is typically store-url/wp-admin
* After logging in click on "Plugins" in the left menu bar
* Click on "Add New" below "Plugins" in the left menu bar 
* Click on the "Upload Plugin" button near the top of the page
* Click on the "Choose file" button near the middle of the page
* Select the TaxConnect.zip file you have been given
* Click on the "Install Now" button
* Click on the "Activate Plugin" button
* Click on "Tax" near the bottom of the left menu bar
* Paste the API key you have been given into the "API Key" field
* Click on the "Save Changes" button

## Installation using wp-cli
```
cd <wordpress-root>/wp-content/plugins
unzip TaxConnect.zip
wp plugin activate TaxConnect
```
* Go to your Admin UI, which is typically store-url/wp-admin
* Click on "Tax" near the bottom of the left menu bar
* Paste the API key you have been given into the "API Key" field
* Click on the "Save Changes" button
